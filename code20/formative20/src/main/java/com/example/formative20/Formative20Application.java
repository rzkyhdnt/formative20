package com.example.formative20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative20Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative20Application.class, args);
	}

}
