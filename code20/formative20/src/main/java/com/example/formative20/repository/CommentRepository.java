package com.example.formative20.repository;

import com.example.formative20.pojo.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, Integer> {
    Comment findById(int id);
    List<Comment> findAll();
    Comment save(Comment comment);
}
