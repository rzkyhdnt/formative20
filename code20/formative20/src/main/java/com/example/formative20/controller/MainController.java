package com.example.formative20.controller;

import com.example.formative20.repository.CommentRepository;
import com.example.formative20.pojo.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MainController {
    @Autowired
    CommentRepository commentRepository;

    public List<Comment> getAllComments(){
        return commentRepository.findAll();
    }

    @ModelAttribute
    public void allComments (Model model){
        List<Comment> comments = new ArrayList<Comment>(getAllComments());
        model.addAttribute("comments", comments);
    }

    @GetMapping("/")
    public ModelAndView show(){
        ModelAndView mav = new ModelAndView("index");
        return mav;
    }

    @GetMapping("/show")
    public ModelAndView showAll(){
        ModelAndView mav = new ModelAndView("index2");
        return mav;
    }

    @PostMapping(value = "/")
    public String addComment(@RequestBody ArrayList<Comment> comment){
        for (Comment cmt : comment) {
            commentRepository.save(cmt);
        }
        return "Success";
    }
}
